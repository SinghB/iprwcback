package com.udemy.resources;

import com.udemy.core.User;
import io.dropwizard.auth.Auth;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloResource {
    @GET //kan ook @POST, @BOOT, @DELETE
    @Produces(MediaType.TEXT_PLAIN) //Produces Plain Text
    public String getGreeting(){
        return "Hello Mah Man";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/secured") //subresource method localhost:8080/hello/secured
    public String getSecuredGreeting(@Auth User user){ //promt user for credentials
        return "Hello Secured Man";
    }
}
